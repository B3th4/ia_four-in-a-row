Small project in python to create an IA that plays four in a row.

It is using the minimax algorithme with alpha/beta prunning.
I also used Multiprocessing to thread the lookup  in the tree.
Next step would be to add incremental depth's to the Algorithme
with a constraint on the time pur turn, as currently it always look at maximum depth of 3

Maybe i'll create an GUI some day to.
