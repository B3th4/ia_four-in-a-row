# -*- coding: utf-8 -*-
"""
Created on Mon Oct  8 11:21:50 2018

@author: A680290
"""

from random import randint,randrange       
import copy
import time
from multiprocessing import Pool
        

BOARDWIDTH = 9
BOARDHEIGHT = 8


MOVESPLAYED = 0

def main():
    board=CreateBoard()
    player1=Human("O") 
    player2=IAMinMax("X","O",3)
    
    printBoard(board)
    CurrentPlayer=player1
    
    gameEnded=False
    message=""
    
#    
#    PlayAction(board,0,"X")
#    PlayAction(board,BOARDWIDTH-1,"X")
#    PlayAction(board,1,"Y")
#    PlayAction(board,BOARDWIDTH-2,"Y")
#    PlayAction(board,2,"Z")
#    PlayAction(board,BOARDWIDTH-3,"Z")           
#    
#    printBoard(board)
    

    
    
    
    start = time.time()
    
    while(not gameEnded):
        if(CurrentPlayer==player1):
            player1.Action(board,CurrentPlayer)
            gameEnded,message= CheckEndGame(board,CurrentPlayer.PlayerPiece)
            
        else:
            player2.Action(board,CurrentPlayer)
            gameEnded,message= CheckEndGame(board,CurrentPlayer.PlayerPiece)
        global MOVESPLAYED
        MOVESPLAYED+=1
        print(MOVESPLAYED)
        if(CurrentPlayer==player1):
            CurrentPlayer=player2
        else:
            CurrentPlayer=player1
        printBoard(board)
    end = time.time()
    print(end - start)
    print(message)

class Human(object):
    def __init__(self, piece):
        self.PlayerPiece = piece

    def Action(self,board,CurrentPlayer):
        x = int(input('input index from 0 to {0}: \n'.format(BOARDWIDTH-1)))
        result=PlayAction(board,x,self.PlayerPiece)
        newElement=result[1]
        while(result[0]==0):
            x = int(input('input index from 0 to {0}: \n'.format(BOARDWIDTH-1)))
            result=PlayAction(board,x,self.PlayerPiece)
            newElement=result[1]
        return newElement
    
    
class IARandom(object):
    def __init__(self, piece):
        self.PlayerPiece = piece

    def Action(self,board,CurrentPlayer):
        verticalIndex=randint(0, 6)
        result=PlayAction(board,verticalIndex, self.PlayerPiece)
        newElement=result[1]
        while(result[0]==0):
            verticalIndex=randint(0, 6)
            result=PlayAction(board,verticalIndex, self.PlayerPiece)
            newElement=result[1]
        return newElement
    
class IAMinMax(object):   
    def __init__(self, piece,enemyPiece,depth=2):
        self.PlayerPiece = piece
        self.enemyPiece = enemyPiece
        self.depth=depth
        self.maxDepth=10
        
    def Action(self,board,CurrentPlayer):
        if(MOVESPLAYED==0):
            result=PlayAction(board,int(BOARDWIDTH/2),self.PlayerPiece)
        
        else:
            succesors=self.GenerateSuccessor(board,self.PlayerPiece)            
            number_of_processes = BOARDWIDTH
            results = Pool(number_of_processes).map(self.threadHelper, succesors)
            length=len(succesors)
            childScores=[None]*length         
#            
#            
#            for index,suc in enumerate(succesors):#            
#                score=self.AlphaBeta(suc[1],self.depth,-10000,10000,self.enemyPiece,suc[0])
#                #score=self.MiniMax(suc[1],self.depth,self.enemyPiece)
#                childScores[index]=(score)
##                
            for index,score in enumerate(results):
                childScores[index]=(score)
            bests=[]
            Bestscore=-1000000
            for score in childScores:
                if score[0] > Bestscore:
                    bests=[score]
                    Bestscore=score[0]
                elif score[0] == Bestscore:
                    bests.append(score)
            
            random_index = randrange(len(bests))
            result=PlayAction(board,bests[random_index][1],self.PlayerPiece)
            
        return result
    
    def IsMirror(self,board):
        for line in board:
            for index in range(int(len(line)/2)):
                if(line[index]!=line[BOARDWIDTH-1-index]):
                    return False
        return True
    
    
    def LegalMoves(self, board):
        possibleMoves=list()
        if self.IsMirror(board):
            for i in range(int(BOARDWIDTH/2)+1):
                if board[0][i]==" ":
                    possibleMoves.append(i)
        else:
            for i in range(0,BOARDWIDTH):
                if board[0][i]==" ":
                    possibleMoves.append(i)
        return possibleMoves
    
    def GenerateSuccessor(self,board,piece):
        legalMoves=self.LegalMoves(board)
        successor=list()
        for move in legalMoves:

            newBoard=copy.deepcopy(board)
            PlayAction(newBoard,move,piece)
            successor.append((move,newBoard))
        return successor
    
    def GetScoreBoard(self,board,PlayerPiece,enemyPiece):
        myScore=0
        opponentScore=0
        for indexRow, row in enumerate(board):
            for indexColum,colum in enumerate(row):
                #if board[indexRow][indexColum] == " ":
                myScore+=self.howManyWinInPosition(board,indexColum,indexRow)
                opponentScore+=self.howManyOpponentWinInPosition(board,indexColum,indexRow)   

        if isWinner(board,self.enemyPiece):
            return -1000
        elif isWinner(board,self.PlayerPiece):
            return 500

        else:
            result=myScore-opponentScore
        return result
        
    def howManyWinInPosition(self,board,x,y):
        
        return HowManyWin(board,self.enemyPiece,x,y)
    
    def howManyOpponentWinInPosition(self,board,x,y):
        
        return HowManyWin(board,self.PlayerPiece,x,y)
        
    def MiniMax(self,board,depth,piece):
        endgame=isWinner(board,self.enemyPiece) or isWinner(board,self.PlayerPiece)
        if(depth == 0 or endgame):
            score=self.GetScoreBoard(board,self.PlayerPiece,self.enemyPiece)

            return score
        elif(piece==self.PlayerPiece):
            childs= self.GenerateSuccessor(board,self.PlayerPiece)
            childScores=[]
            for child in childs:
                newValue = self.MiniMax(child[1],depth-1,self.enemyPiece)
                childScores.append(newValue)
            return max(childScores)
            
        else:
            childs= self.GenerateSuccessor(board,self.enemyPiece)
            childScores=[]
            for child in childs:
                newValue = self.MiniMax(child[1],depth-1,self.PlayerPiece)
                childScores.append(newValue)
            return min(childScores)
        
    def AlphaBeta(self,board,depth,alpha,beta,piece,index):
        newAlpha=alpha
        newBeta=beta
        endgame=isWinner(board,self.enemyPiece) or isWinner(board,self.PlayerPiece)
        if(depth == 0 or endgame):
            score=self.GetScoreBoard(board,self.PlayerPiece,self.enemyPiece)
            return (score,index)
        
        elif(piece==self.PlayerPiece):
            childs= self.GenerateSuccessor(board,self.PlayerPiece)
            childScores=[]
            for child in childs:
                newValue = self.AlphaBeta(child[1],depth-1,newAlpha,newBeta,self.enemyPiece,index)
                childScores.append(newValue)
            value= max(childScores)
            
            if value[0] >= newBeta:
                return value
            
            newAlpha= max(newAlpha,value[0])
            return value
        
            
        else:
            childs= self.GenerateSuccessor(board,self.enemyPiece)
            childScores=[]
            for child in childs:
                newValue = self.AlphaBeta(child[1],depth-1,newAlpha,newBeta,self.PlayerPiece,index)
                childScores.append(newValue)
            value= min(childScores)
            
            
            if value[0] <= newAlpha:
                return value
            
            newBeta = min(newBeta,value[0])
            return value
        
    
        
        
    def threadHelper(self,succesor):
        score=self.AlphaBeta(succesor[1],self.depth,-10000,10000,self.enemyPiece,succesor[0])
        return(score)

        
    
def CreateBoard():
    return [[" " for j in range(BOARDWIDTH)] for i in range(BOARDHEIGHT)]

def PlayAction(_board,verticalIndex, player):
    horizontalIndex=BOARDHEIGHT-1
    played= False
    while(not played):
        if(verticalIndex<0 or verticalIndex>=BOARDWIDTH):
            return(0, "invalidIndex")
        elif(horizontalIndex < 0):
            return (0,"choose another column")
        elif(_board[horizontalIndex][verticalIndex]==" "):
            _board[horizontalIndex][verticalIndex]=player
            played= True
            return (1,(player,horizontalIndex,verticalIndex))
        else:
            horizontalIndex-=1
        
def CheckEndGame(board,newElement):
    if(isWinner(board,newElement[0])):
        return (True,newElement[0]+" is the winner!")
    elif(MOVESPLAYED==BOARDWIDTH*BOARDHEIGHT):
        return (True,"Draw")
    else:
        return (False,"Not Ended yet")

def isWinner(board, player):
    # check horizontal spaces
    for y in range(BOARDHEIGHT): 
        for x in range(BOARDWIDTH - 3):
            if board[y][x] == player and board[y][x+1] == player and board[y][x+2] == player and board[y][x+3] == player:
                return True

    # check vertical spaces
    for x in range(BOARDWIDTH):
        for y in range(BOARDHEIGHT - 3):
            if board[y][x] == player and board[y+1][x] == player and board[y+2][x] == player and board[y+3][x] == player:
                return True

    # check  diagonal1 spaces
    for x in range(BOARDWIDTH - 3):
        for y in range(BOARDHEIGHT - 3) :
            if board[y][x] == player and board[y+1][x+1] == player and board[y+2][x+2] == player and board[y+3][x+3] == player:
                return True

    # check  diagonal2 spaces
    for x in range(BOARDWIDTH - 3):
        for y in range(3, BOARDHEIGHT):
            if board[y][x] == player and board[y-1][x+1] == player and board[y-2][x+2] == player and board[y-3][x+3] == player:
                return True

    return False

def HowManyWin(board,piece,x,y):
    
    horizontal=canStillWinHorizontal(board,piece,y,x)
    vertical=canStillWinVertical(board,piece,y,x)
    diagonal1=canStillWinDiag1(board,piece,x,y)
    diagonal2=canStillWinDiag2(board,piece,x,y)
    
    return horizontal+vertical+diagonal1+diagonal2

   
def canStillWinHorizontal(board,piece,verticalIndex,horizontalIndex):
    horizontalLower=max(0,horizontalIndex-3)
    winnable=0
    
    for low in range(horizontalLower,horizontalIndex+1):
        count=0
        horizontalHigh=min(BOARDWIDTH-1,low+3)
        for high in range(low,horizontalHigh+1):
            if(board[verticalIndex][high]!=piece):
                count=count+1
                if count>=4:
                    winnable+=1
    return winnable   

def canStillWinVertical(board,piece,verticalIndex,horizontalIndex):
    verticalLower=max(0,verticalIndex-3)
    winnable=0
    
    for low in range(verticalLower,verticalIndex+1):
        count=0
        verticalHigh=min(BOARDHEIGHT-1,low+3)
        for high in range(low,verticalHigh+1):
            if(board[high][horizontalIndex]!=piece):
                count=count+1
                if count>=4:
                    winnable+=1
    return winnable

def canStillWinDiag1(board,piece,verticalIndex,horizontalIndex):
    winnable=0
    lowerX,lowerY=verticalIndex,horizontalIndex
    steps=3
    steps2=1
    
    while(lowerX>0 and lowerY<BOARDHEIGHT-1 and steps>0):
        lowerX=lowerX-1
        lowerY=lowerY+1
        steps=steps-1
        steps2=steps2+1
        
    for index in range(steps2):
        count=0
        wrong=False

        for end in range(index,index+4):
            if(not wrong):

                verticalEnd=lowerX+index+3
                horizontalEnd=lowerY-index-3
                if(verticalEnd < BOARDWIDTH and horizontalEnd >=0):                       
                    if(board[lowerY-end][lowerX+end]!=piece):
                        count=count+1
                        if count>=4:
                            winnable+=1
                    else:
                        wrong=True
    return winnable
        
def canStillWinDiag2(board,piece,verticalIndex,horizontalIndex):
    winnable=0
    lowerX,lowerY=verticalIndex,horizontalIndex
    steps=3
    steps2=1
    
    while(lowerX>0 and lowerY>0 and steps>0):
        lowerX=lowerX-1
        lowerY=lowerY-1
        steps=steps-1
        steps2=steps2+1
        
    for index in range(steps2):
        count=0
        wrong=False

        for end in range(index,index+4):
            if(not wrong):

                verticalEnd=lowerX+index+3
                horizontalEnd=lowerY+index+3
                
                if(verticalEnd < BOARDWIDTH and horizontalEnd < BOARDHEIGHT):                       
                    if(board[lowerY+end][lowerX+end]!=piece):
                        count=count+1
                        if count>=4:
                            winnable+=1
                    else:
                        wrong=True
    return winnable
    
        
def printBoard(board):
    for line in board:
        print("-----------------------------------------------------------")
        lineStr="|  "
        for elem in line:
            lineStr+=elem+"  |  "
        print(lineStr)
    print("============================================================")
    
if __name__ == '__main__':
    main()